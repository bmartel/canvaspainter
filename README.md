
Usage
=====

Register the canvas:

```js
canvasStack.createInstance("canvasid");
var canvasObj = canvasStack.getInstance("canvasid");
```

Set Background color:

```js
canvasObj.setBackgroundColor("#333333");
```

Create an object:

```js
var rect1 = canvasPainter.createRect("rect1",0,0,50,50,"#ffffff",0,"");
```

Draw canvas:

```js
canvasObj.update();
canvasObj.clear();
canvasObj.render();
```