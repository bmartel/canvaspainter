/**************************************************************************************************
 * SimpleCanvasObjects - Circle
 *
 * @copyright Brandon Martel 2013
 *
 * Basic Canvas Object that defines properties for rendering ellipse shapes
 *
 **************************************************************************************************/
function CCircle(id,cX, cY,radius,startAngle, endAngle, antiClockwise,fillColor,lineWidth,strokeColor){
    this.id = id;
    this.cX = cX;
    this.cY = cY;
    this.dX = 0;
    this.dY = 0;
    this.radius = radius;
    this.startAngle = startAngle;
    this.endAngle = endAngle;
    this.antiClockwise = antiClockwise;
    this.fillColor = fillColor;
    this.lineWidth = lineWidth;
    this.strokeColor = strokeColor;
    this.drawAsStroke = true;
    this.drawAsFill = false;
    this.fillGradient = false;
    this.openPath = false;
}
CCircle.prototype.render =function(context){

    if(this.fillGradient){
        var grd=context.createRadialGradient(this.cX,this.cY,5,this.cX + 30,this.cY - 15,100);
        grd.addColorStop(0,"red");
        grd.addColorStop(1,"blue");

        // Fill with gradient
        context.fillStyle=grd;
    }else{
        context.fillStyle = this.fillColor;
    }

    context.lineWidth = this.lineWidth; //define stroke width
    context.strokeStyle = this.strokeColor; //define stroke color

    context.beginPath();

    //define the starting angle at 0 and ending angle at 2*Math.PI
    context.arc(this.cX, this.cY, this.radius, this.startAngle, this.endAngle, this.antiClockwise);

    if(!this.openPath)
        context.closePath();



    //Check whether a fill color is supplied
    if(this.fillColor && this.drawAsFill){
        context.fill(); //fill circle
    }

    //Check whether the stroke color and line width is supplied
    if(this.strokeColor && this.lineWidth && this.drawAsStroke){
        context.stroke();
    }

};
CCircle.prototype.update = function(context){

    this.cX += this.dX;
    this.cY += this.dY;

};
CCircle.prototype.fillAndStrokeObj = function(){
    this.drawAsStroke = true;
    this.drawAsFill = true;
};
CCircle.prototype.fillObj = function(){
    this.drawAsStroke = false;
    this.drawAsFill = true;
};
CCircle.prototype.strokeObj = function(){
    this.drawAsStroke = true;
    this.drawAsFill = false;
};
CCircle.prototype.setGradientFill = function(){
    this.fillGradient = true;
};
CCircle.prototype.setFill = function(fill){
    this.fillColor = fill;
};
CCircle.prototype.setStroke = function(stroke){
    this.strokeColor = stroke;
};
CCircle.prototype.setStrokeWidth = function(width){
    this.lineWidth = width;
};
CCircle.prototype.setOpenPath = function(){
    this.openPath = true;
};
CCircle.prototype.setX = function(x){
    this.cX = x;
};
CCircle.prototype.setY = function(y){
    this.cY = y;
};
CCircle.prototype.setdX = function(x){
    this.dX = x;
};
CCircle.prototype.setdY = function(y){
    this.dY = y;
};
CCircle.prototype.setStartAngle = function(ang){
    this.startAngle = ang;
};
CCircle.prototype.setEndAngle = function(ang){
    this.endAngle = ang;
};
CCircle.prototype.registerAnimation =function(animation){
    this.animation = animation;
};
CCircle.prototype.setRadius = function(radius){
    this.radius = radius;
};
CCircle.prototype.getRadius = function(){
    return this.radius;
};
CCircle.prototype.getX = function(){
    return this.cX;
};
CCircle.prototype.getY = function(){
    return this.cY;
};
CCircle.prototype.getId = function(){
    return this.id;
};


/**************************************************************************************************
 * SimpleCanvasObjects - Rectangle
 *
 * @copyright Brandon Martel 2013
 *
 * Basic Canvas Object that defines properties for rendering rectangle shapes
 *
 **************************************************************************************************/
function CRectangle(id,cX, cY,width,height,fillColor,lineWidth,strokeColor){
    this.id = id;
    this.cX = cX;
    this.cY = cY;
    this.dX = 0;
    this.dY = 0;
    this.width = width;
    this.height = height;
    this.fillColor = fillColor;
    this.lineWidth = lineWidth;
    this.strokeColor = strokeColor;
    this.drawAsStroke = true;
    this.drawAsFill = false;

}
CRectangle.prototype.render =function(context){
    context.fillStyle = this.fillColor;
    context.lineWidth = this.lineWidth; //define stroke width
    context.strokeStyle = this.strokeColor; //define stroke color

    context.rect(this.cX,this.cY,this.width,this.height);


    //Check whether a fill color is supplied
    if(this.fillColor && this.drawAsFill){
        context.fillRect(this.cX,this.cY,this.width,this.height); //fill rectangle
    }

    //Check whether the stroke color and line width is supplied
    if(this.strokeColor && this.lineWidth && this.drawAsStroke){

        context.stroke();
    }

};
CRectangle.prototype.update = function(context){

    this.cX += this.dX;
    this.cY += this.dY;

};
CRectangle.prototype.fillObj = function(){
    this.drawAsStroke = false;
    this.drawAsFill = true;
};
CRectangle.prototype.strokeObj = function(){
    this.drawAsStroke = true;
    this.drawAsFill = false;
};
CRectangle.prototype.fillAndStrokeObj = function(){
    this.drawAsStroke = true;
    this.drawAsFill = true;
};
CRectangle.prototype.setFill = function(fill){
    this.fillColor = fill;
};
CRectangle.prototype.setStroke = function(stroke){
    this.strokeColor = stroke;
};
CRectangle.prototype.setStrokeWidth = function(width){
    this.lineWidth = width;
};
CRectangle.prototype.setX = function(x){
    this.cX = x;
};
CRectangle.prototype.setY = function(y){
    this.cY = y;
};
CRectangle.prototype.setdX = function(x){
    this.dX = x;
};
CRectangle.prototype.setdY = function(y){
    this.dY = y;
};
CRectangle.prototype.registerAnimation =function(animation){
    this.animation = animation;
};
CRectangle.prototype.setWidth = function(width){
    this.width = width;
};
CRectangle.prototype.getWidth = function(){
    return this.width;
};

CRectangle.prototype.setHeight = function(height){
    this.height = height;
};
CRectangle.prototype.getHeight = function(){
    return this.height;
};
CRectangle.prototype.getX = function(){
    return this.cX;
};
CRectangle.prototype.getY = function(){
    return this.cY;
};
CRectangle.prototype.getId = function(){
    return this.id;
};

/**************************************************************************************************
 * SimpleCanvasObjects - BezierCurve
 *
 * @copyright Brandon Martel 2013
 *
 * Basic Canvas Object that defines properties for rendering bezier curve shapes
 *
 **************************************************************************************************/
function CBezierCurve(id,cX, cY,bezierPoints,fillColor,lineWidth,strokeColor){
    this.id = id;
    this.cX = cX;
    this.cY = cY;
    this.dX = 0;
    this.dY = 0;
    this.bezierPoints = bezierPoints; // bezierPoints[pSet1[p1,p2,p3,p4,p5,p6], pSet2[p1,p2,p3,p4,p5,p6] ];
    this.fillColor = fillColor;
    this.lineWidth = lineWidth;
    this.strokeColor = strokeColor;
    this.drawAsStroke = true;
    this.drawAsFill = false;
    this.openPath = false;
}
CBezierCurve.prototype.render =function(context){

    context.beginPath();
    context.moveTo(this.cX, this.cY);

    for(var i = 0; i < this.bezierPoints.length; i++){
        context.bezierCurveTo(this.bezierPoints[i][0],this.bezierPoints[i][1],this.bezierPoints[i][2],this.bezierPoints[i][3],this.bezierPoints[i][4],this.bezierPoints[i][5]);
//        console.log("Points: " +this.bezierPoints[i][0]+", "+this.bezierPoints[i][1]+", "+this.bezierPoints[i][2]+", "+this.bezierPoints[i][3]+", "+this.bezierPoints[i][4]+", "+this.bezierPoints[i][5] );
    }

    if(!this.openPath)
        context.closePath();

    if(this.fillGradient){
        var grd=context.createRadialGradient(this.cX,this.cY,5,this.cX + 30,this.cY - 15,100);
        grd.addColorStop(0,"blue");
        grd.addColorStop(1,"gray");

        // Fill with gradient
        context.fillStyle=grd;
    }else{
        context.fillStyle = this.fillColor;
    }

    context.lineWidth = this.lineWidth; //define stroke width
    context.strokeStyle = this.strokeColor; //define stroke color

    //Check whether a fill color is supplied
    if(this.fillColor && this.drawAsFill){
        context.fill(); //fill circle
    }

    //Check whether the stroke color and line width is supplied
    if(this.strokeColor && this.lineWidth && this.drawAsStroke){
        context.stroke();
    }

};
CBezierCurve.prototype.update = function(context){

    this.cX += this.dX;
    this.cY += this.dY;

};
CBezierCurve.prototype.fillAndStrokeObj = function(){
    this.drawAsStroke = true;
    this.drawAsFill = true;
};
CBezierCurve.prototype.fillObj = function(){
    this.drawAsStroke = false;
    this.drawAsFill = true;
};
CBezierCurve.prototype.strokeObj = function(){
    this.drawAsStroke = true;
    this.drawAsFill = false;
};
CBezierCurve.prototype.setGradientFill = function(){
    this.fillGradient = true;
};
CBezierCurve.prototype.setFill = function(fill){
    this.fillColor = fill;
};
CBezierCurve.prototype.setStroke = function(stroke){
    this.strokeColor = stroke;
};
CBezierCurve.prototype.setStrokeWidth = function(width){
    this.lineWidth = width;
};
CBezierCurve.prototype.setOpenPath = function(){
    this.openPath = true;
};

/**************************************************************************************************
 * SimpleCanvasObjects - Image
 *
 * @copyright Brandon Martel 2013
 *
 * Basic Canvas Object that defines properties for rendering images
 *
 **************************************************************************************************/
function CImage(id,url,cX, cY, width, height){
    this.id = id;
    this.url = url;
    this.img_cache = new Image();
    this.imageLoaded = false;
    this.loadImage(url);
    this.cX = cX;
    this.cY = cY;
    this.width = this.img_cache.width;
    this.height = this.img_cache.height;
    this.dX = 0;
    this.dY = 0;
    this.flipHorizontal = false;
    this.flipVertical = false;
}
CImage.prototype.render = function(context){
    if(this.imageLoaded){
        if(this.flipVertical){
            context.translate(0, this.height * -1);
            context.scale(1, -1);
        }
        if(this.flipHorizontal){
            context.translate(this.width * -1, 0);
            context.scale(-1, 1);
        }
        context.drawImage(this.img_cache, this.cX, this.cY);
    }

};
CImage.prototype.update = function(context){

    this.cX += this.dX;
    this.cY += this.dY;

};
CImage.prototype.loadImage = function(url){
    this.imageLoaded = false;

    var me = this; //Get local reference

    this.img_cache.onload = function(){
        me.imageLoaded = true;
    }

    if(url){
        this.url = url;

    }
    this.img_cache.src = this.url;
};
CImage.prototype.flip = function(direction){
    if(direction.toLowerCase() == "horizontal") this.flipHorizontal = true;
    if(direction.toLowerCase() == "vertical") this.flipVertical= true;
};
CImage.prototype.setX = function(x){
    this.cX = x;
};
CImage.prototype.setY = function(y){
    this.cY = y;
};
CImage.prototype.setdX = function(x){
    this.dX = x;
};
CImage.prototype.setdY = function(y){
    this.dY = y;
};

/**************************************************************************************************
 * SimpleCanvasObjects - Text
 *
 * @copyright Brandon Martel 2013
 *
 * Basic Canvas Object that defines properties for rendering text
 *
 **************************************************************************************************/
function CText(id,textStr,fontSize,fontFamily,fontColor,cX,cY){
    this.id = id;
    this.textStr = textStr;
    this.fontSize = fontSize;
    this.fontFamily = fontFamily;
    this.fontColor = fontColor;
    this.cX = cX;
    this.cY = cY;
}
CText.prototype.render = function(context){
    context.font = this.fontSize +' '+this.fontFamily;
    context.fillStyle = this.fontColor;
    context.fillText(this.textStr, this.cX, this.cY);
};
CText.prototype.update = function(context){

    this.cX += 0;
    this.cY += 0;

};
CText.prototype.moveTo = function(x,y){

    this.cX = x;
    this.cY = y;
};
CText.prototype.setColor = function(color){
    this.fontColor = color;
};
CText.prototype.setFontFamily = function(fontFamily){
    this.fontFamily = fontFamily;
};
CText.prototype.setFontSize = function(fontSize){
    this.fontSize = fontSize;
};
CText.prototype.text = function(textStr){
    this.textStr = textStr;
};

/**************************************************************************************************
 * SimpleCanvasObjects - line
 *
 * @copyright Brandon Martel 2013
 *
 * Basic Canvas Object that defines properties for rendering line shapes
 *
 **************************************************************************************************/
function CLine(id,cX, cY,linePoints,fillColor,lineWidth,strokeColor){
    this.id = id;
    this.cX = cX;
    this.cY = cY;
    this.dX = 0;
    this.dY = 0;
    this.linePoints = linePoints; // linePoints[ pSet1[p1,p2], pSet2[p1,p2] ];
    this.fillColor = fillColor;
    this.lineWidth = lineWidth;
    this.strokeColor = strokeColor;
    this.drawAsStroke = true;
    this.drawAsFill = false;
    this.openPath = false;
}
CLine.prototype.render =function(context){

    context.beginPath();
    context.moveTo(this.cX, this.cY);

    for(var i = 0; i < this.linePoints.length; i++){
        context.lineTo(this.linePoints[i][0],this.linePoints[i][1]);
//        console.log("Points: " +this.linePoints[i][0]+", "+this.linePoints[i][1]);
    }

    if(!this.openPath)
        context.closePath();


    context.fillStyle = this.fillColor;

    context.lineWidth = this.lineWidth; //define stroke width
    context.strokeStyle = this.strokeColor; //define stroke color

    //Check whether a fill color is supplied
    if(this.fillColor && this.drawAsFill){
        context.fill(); //fill circle
    }

    //Check whether the stroke color and line width is supplied
    if(this.strokeColor && this.lineWidth && this.drawAsStroke){
        context.closePath();
        context.stroke();
    }

};
CLine.prototype.update = function(context){

    this.cX += this.dX;
    this.cY += this.dY;

};
CLine.prototype.fillAndStrokeObj = function(){
    this.drawAsStroke = true;
    this.drawAsFill = true;
};
CLine.prototype.fillObj = function(){
    this.drawAsStroke = false;
    this.drawAsFill = true;
};
CLine.prototype.strokeObj = function(){
    this.drawAsStroke = true;
    this.drawAsFill = false;
};
CLine.prototype.setGradientFill = function(){
    this.fillGradient = true;
};
CLine.prototype.setFill = function(fill){
    this.fillColor = fill;
};
CLine.prototype.setStroke = function(stroke){
    this.strokeColor = stroke;
};
CLine.prototype.setStrokeWidth = function(width){
    this.lineWidth = width;
};
CLine.prototype.setOpenPath = function(){
    this.openPath = true;
};

/**************************************************************************************************
 * CanvasPainter
 *
 * @copyright Brandon Martel 2013
 *
 * Creates an object which binds to a canvas and enables simple animations and rendering of shapes.
 *
 **************************************************************************************************/
function CanvasPainter(canvasId) {
    this.activeObjects = {};
    this.canvas = document.getElementById(canvasId);
    this.context = this.canvas.getContext("2d");
    this.width = this.canvas.width;
    this.height = this.canvas.height;
    this.cells = new Array(this.width * this.height);
    this.mousePos = {X:0, Y:0};
    this.running = false;
    this.frameClear = true;
    this.renderBackground = true;
    this.createRect("bg",0,0,this.width, this.height, "#FFFFFF" ,0,""); //Create default white canvas background
    this.activeObjects["bg"].fillObj(); //Fill canvas bg
}
CanvasPainter.prototype.animate = function(){
    var me = this; //Get canvasPainter reference

    //Perform the animations only if they have been activated
    if(this.running)
    {
        this.update(); //Perform calculations and object movement
        this.clear();  //Clear the canvas
        this.render();   //Redraw the canvas objects

//        console.log("Animating!");
        //Loop over the animation until it is stopped
        setTimeout(function(){
            me.animate();
        }, 40); //25 fps
    }
};
CanvasPainter.prototype.removeObj = function(id){
    delete this.activeObjects[id];
};
CanvasPainter.prototype.createCircle = function(id,cX, cY,radius,startAngle, endAngle, antiClockwise,fillColor,lineWidth,strokeColor){
    return this.activeObjects[id] = new CCircle(id,cX, cY,radius,startAngle, endAngle, antiClockwise,fillColor,lineWidth,strokeColor);
};
CanvasPainter.prototype.createRect = function(id,cX, cY,width,height,fillColor,lineWidth,strokeColor){
    return this.activeObjects[id] = new CRectangle(id,cX, cY,width,height,fillColor,lineWidth,strokeColor);
};
CanvasPainter.prototype.createBezier = function(id,cX, cY,bezierPoints,fillColor,lineWidth,strokeColor){
    return this.activeObjects[id] = new CBezierCurve(id,cX, cY,bezierPoints,fillColor,lineWidth,strokeColor);
};
CanvasPainter.prototype.createLine = function(id,cX, cY,linerPoints,fillColor,lineWidth,strokeColor){
    return this.activeObjects[id] = new CLine(id,cX, cY,linerPoints,fillColor,lineWidth,strokeColor);
};
CanvasPainter.prototype.createImage = function(id,url,cX, cY,width,height){
    return this.activeObjects[id] = new CImage(id,url,cX, cY,width,height);
};
CanvasPainter.prototype.createText = function(id,textStr,fontSize,fontFamily,fontColor,cX,cY){
    return this.activeObjects[id] = new CText(id,textStr,fontSize,fontFamily,fontColor,cX,cY);
};
CanvasPainter.prototype.setBackgroundColor = function(color){
    this.activeObjects["bg"].setFill(color);
};
CanvasPainter.prototype.clear = function(){
//    console.log("Clearing Canvas");
    if(this.frameClear)
        this.context.clearRect(0, 0, this.width, this.height);
};
CanvasPainter.prototype.getBaseContext = function(){
    return this.context;
};
CanvasPainter.prototype.getCanvasObjById = function(id){
    return this.activeObjects[id];
};
CanvasPainter.prototype.getMousePos = function(){
    return this.mousePos;
};
CanvasPainter.prototype.registerEventListener =function(type,fx){
    if(type =="mousemove" && fx == null){
        var me = this; //Get local reference to CanvasPainter
        //register mousemove to be able to track coordinates of the mouse on the canvas
        this.canvas.addEventListener(type, function(e){
            me.setMousePos(e);
        });
    }else{
        this.canvas.addEventListener(type,fx);
    }

};
CanvasPainter.prototype.render = function(){
    //Render Canvas background color
    if(this.renderBackground || this.frameClear){
        this.activeObjects["bg"].render(this.context);
        this.renderBackground = false;
    }

    for(var canvasObj in this.activeObjects){
        if(canvasObj != "bg")
            this.activeObjects[canvasObj].render(this.context);
    }
};
CanvasPainter.prototype.toggleFrameClear = function(){
    console.log(this.frameClear);
    this.frameClear = !this.frameClear;
};
CanvasPainter.prototype.setMousePos = function(e){
    var rect = this.canvas.getBoundingClientRect();
    this.mousePos.X = e.clientX - rect.left;
    this.mousePos.Y = e.clientY - rect.top;
};
CanvasPainter.prototype.update = function(){
//    console.log("Updating Canvas");
    for(var canvasObj in this.activeObjects){
        this.activeObjects[canvasObj].update(this.context);
    }
};
CanvasPainter.prototype.moveTo =function(objId,dX,dY){
    var localObj = this.getCanvasObjById(objId);

    localObj.setX(dX);
    localObj.setY(dY);

};
CanvasPainter.prototype.start =function(){
    this.running = true;
}
CanvasPainter.prototype.stop =function(){
    this.running = false;
}
CanvasPainter.prototype.valueAt = function(point) {
    return this.cells[point.y * this.width + point.x];
};
CanvasPainter.prototype.setValueAt = function(point, value) {
    this.cells[point.y * this.width + point.x] = value;
};
CanvasPainter.prototype.isInside = function(point) {
    return point.x >= 0 && point.y >= 0 &&
        point.x < this.width && point.y < this.height;
};
CanvasPainter.prototype.moveValue = function(from, to) {
    this.setValueAt(to, this.valueAt(from));
    this.setValueAt(from, undefined);
};

/**************************************************************************************************
 * CanvasStack singleton
 *
 * @copyright Brandon Martel 2013
 *
 * A singleton class for handling canvas objects
 *
 **************************************************************************************************/
//
function CanvasStack() {
    this.canvas;
};
CanvasStack.prototype.createInstance = function(id){
    this.canvas = new CanvasPainter(id);
};
CanvasStack.prototype.getInstance = function(id){
    return this.canvas;
};
CanvasStack.prototype.update = function(){
    this.canvas.update();
};

//Instantiate the singleton CanvasStack
var canvasStack = new CanvasStack();
